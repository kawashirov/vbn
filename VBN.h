#ifndef VOABIGNUM_H
#define VOABIGNUM_H

#include <istream>
#include <ostream>
#include <utility>
#include <openssl/bn.h>

/**
	Проблематично одновременно определить операторы для BN_ULONG и BN_LONG - возникает неопределенност. Если
	определен VBN_DEFAULT_UNSIGNED, то все не-BIGNUM операторы, будут использовать BN_ULONG вместо BN_LONG.
	BN_LONG удобнее, чем BN_ULONG, но BN_ функции не используют BN_LONG. VBN_DEFAULT_UNSIGNED не влияет на методы
	клсса VBN. Методы определены и для BN_ULONG (оканчиваются на 'u' - "unsigned"), и для BN_LONG (оканчиваются
	на 's' - "signed"). Взависимости от того, задан ли VBN_DEFAULT_UNSIGNED, устанавливается VBN_LONG.
*/
#ifdef VBN_DEFAULT_UNSIGNED
#define VBN_LONG BN_ULONG
#else
#define VBN_LONG BN_LONG
#endif // VBN_DEFAULT_UNSIGNED

/**
		VBN поддерживает возможности С++11, для управления этим используется VBN_CPP11. Если VBN_CPP11 будет
	определен как true, то возможности будут включены, если будет определен как false - нет. Если не будет
	определен, то он определится автоматически исходя из __cplusplus.
*/
#ifndef VBN_CPP11
#if __cplusplus >= 201103L
#define VBN_CPP11 true
#else
#define VBN_CPP11 false
#endif // __cplusplus >= 201103L
#endif // ndef VBN_CPP11

#if VBN_CPP11
#define NURUPO nullptr
#else
#define NURUPO NULL
#endif // VBN_CPP11 == YES

/**
    VBN - voaoom's BIGNUM
*/
class VBN {
public:
	BIGNUM * bn;
public:
	VBN();
	VBN(const BIGNUM * bn);
	VBN(const char * str);
	VBN(const VBN & other);
	VBN(const VBN_LONG & other);
	~VBN();

	VBN & add(const VBN & addend);
	VBN & addu(const BN_ULONG & addend);
	VBN & adds(const BN_LONG & addend);
	VBN & mod_add(const VBN & addend, const VBN & modulus, BN_CTX * ctx);
	VBN & mod_add(const VBN & addend, const VBN & modulus);
	friend VBN add(const VBN & augend, const VBN & addend);
	friend VBN addu(const VBN & augend, const BN_ULONG & addend);
	friend VBN adds(const VBN & augend, const BN_LONG & addend);
	friend VBN mod_add(const VBN & augend, const VBN & addend, const VBN & modulus, BN_CTX * ctx);
	friend VBN mod_add(const VBN & augend, const VBN & addend, const VBN & modulus);

	VBN & sub(const VBN & subtrahend);
	VBN & subu(const BN_ULONG & subtrahend);
	VBN & subs(const BN_LONG & subtrahend);
	VBN & mod_sub(const VBN & subtrahend, const VBN & modulus, BN_CTX * ctx);
	VBN & mod_sub(const VBN & subtrahend, const VBN & modulus);
	friend VBN sub(const VBN & minuend, const VBN & subtrahend);
	friend VBN subu(const VBN & minuend, const BN_ULONG & subtrahend);
	friend VBN subs(const VBN & minuend, const BN_LONG & subtrahend);
	friend VBN mod_sub(const VBN & minuend, const VBN & subtrahend, const VBN & modulus, BN_CTX * ctx);
	friend VBN mod_sub(const VBN & minuend, const VBN & subtrahend, const VBN & modulus);

	VBN & mul(const VBN & multiplier, BN_CTX * ctx);
	VBN & mulu(const BN_ULONG & multiplier);
	VBN & muls(const BN_LONG & multiplier);
	VBN & mod_mul(const VBN & multiplier, const VBN & modulus, BN_CTX * ctx);
	VBN & mul(const VBN & multiplier);
	VBN & mod_mul(const VBN & multiplier, const VBN & modulus);
	friend VBN mul(const VBN & multiplicand, const VBN & multiplier);
	friend VBN mul(const VBN & multiplicand, const VBN & multiplier, BN_CTX * ctx);
	friend VBN mulu(const VBN & multiplicand, const BN_ULONG & multiplier);
	friend VBN muls(const VBN & multiplicand, const BN_LONG & multiplier);
	friend VBN mod_mul(const VBN & multiplicand, const VBN & multiplier, const VBN & modulus, BN_CTX * ctx);
	friend VBN mod_mul(const VBN & multiplicand, const VBN & multiplier, const VBN & modulus);

	VBN & div(const VBN & divisor, BN_CTX * ctx);
	VBN & divu(const BN_ULONG & divisor);
	VBN & divs(const BN_LONG & divisor);
	VBN & div(const VBN & divisor);
	friend VBN div(const VBN & dividend, const VBN & divisor);
	friend VBN div(const VBN & dividend, const VBN & divisor, BN_CTX * ctx);
	friend VBN divu(const VBN & dividend, const BN_ULONG & divisor);
	friend VBN divs(const VBN & dividend, const BN_LONG & divisor);

	VBN & mod(const VBN & modulus, BN_CTX * ctx);
	VBN & mod(const VBN & modulus);
	BN_ULONG modu(const BN_ULONG & modulus) const;
	BN_ULONG mods(const BN_LONG & modulus) const;
	friend VBN mod(const VBN & dividend, const VBN & modulus, BN_CTX * ctx);
	friend VBN mod(const VBN & dividend, const VBN & modulus);
	friend BN_ULONG modu(const VBN & dividend, const BN_ULONG & modulus);
	friend BN_ULONG mods(const VBN & dividend, const BN_LONG & modulus);

	VBN & exp(const VBN & exponent, BN_CTX * ctx);
	VBN & mod_exp(const VBN & exponent, const VBN & modulus, BN_CTX * ctx);
	VBN & exp(const VBN & exponent);
	VBN & mod_exp(const VBN & exponent, const VBN & modulus);
	friend VBN exp(const VBN & base, const VBN & exponent);
	friend VBN exp(const VBN & base, const VBN & exponent, BN_CTX * ctx);
	friend VBN mod_exp(const VBN & base, const VBN & exponent, const VBN & modulus);
	friend VBN mod_exp(const VBN & base, const VBN & exponent, const VBN & modulus, BN_CTX * ctx);

	VBN & sqr();
	VBN & mod_sqr(const VBN & modulus);
	VBN sqr(const VBN & a);
	VBN sqr(const VBN & a, const VBN & modulus);

	VBN & gcd(const VBN & b);
	VBN & lcm(const VBN & b);
	friend VBN gcd(const VBN & a, const VBN & b);
	friend VBN lcm(const VBN & a, const VBN & b);

	VBN & neg();
	VBN & inc();
	VBN & dec();
	friend VBN neg(const VBN & a);
	friend VBN inc(const VBN & a);
	friend VBN dec(const VBN & a);

	bool is_zero() const;
	bool is_one() const;
	bool is_odd() const;
	bool is(const BN_ULONG & u_word) const;
	bool is(const BN_LONG & s_word) const;

	VBN & rshift(int b);
	VBN & lshift(int b);
	VBN & rshift1();
	VBN & lshift1();
	friend VBN rshift(const VBN & a, int b);
	friend VBN lshift(const VBN & a, int b);
	friend VBN rshift1(const VBN & a);
	friend VBN lshift1(const VBN & a);

	VBN & read(std::istream & in);
	VBN & write(std::ostream & out);

	/* Особые случаи */

	// Поддержка C++11
#if VBN_CPP11
	VBN(VBN && temp);
	VBN & operator=(VBN && temp);
	friend VBN operator"" _BN(const char * str) { return VBN(str); }
#endif // VBN_CPP11

	// Операторы С++
#ifndef VBN_DISABLE_ALL_OPERATORS
	VBN & operator=(const BIGNUM * bn);
	VBN & operator=(const char * str);
	VBN & operator=(const VBN & other);

	operator BIGNUM & () const;
	operator BIGNUM * () const;
	operator BIGNUM * const * () const;
	operator bool () const;

	friend VBN operator+(const VBN & augend, const VBN & addend);
	VBN & operator+=(const VBN & addend);

	friend VBN operator-(const VBN & minuend, const VBN & subtrahend);
	VBN & operator-=(const VBN & subtrahend);

	friend VBN operator*(const VBN & multiplicand, const VBN & multiplier);
	VBN & operator*=(const VBN & multiplier);

	friend VBN operator/(const VBN & dividend, const VBN & divisor);
	VBN & operator/=(const VBN & divisor);

	friend VBN operator%(const VBN & dividend, const VBN & modulus);
	VBN & operator%=(const VBN & modulus);

	// power operator
	friend VBN operator^(const VBN & base, const VBN & exponent);
	VBN & operator^=(const VBN & exponent);

	// "&" - gcd, "|" - lcm
	friend VBN operator&(const VBN & a, const VBN & b);
	friend VBN operator|(const VBN & a, const VBN & b);
	VBN & operator&=(const VBN & b);
	VBN & operator|=(const VBN & b);

	friend VBN operator-(const VBN & a);
	VBN & operator++();
	VBN & operator--();
	friend VBN operator++(VBN & a, int);
	friend VBN operator--(VBN & a, int);

	bool operator!() const;

	friend bool operator==(const VBN & a, const VBN & b);
	friend bool operator==(const VBN & a, const BN_ULONG & b);
	friend bool operator==(const BN_ULONG & a, const VBN & b);
	friend bool operator!=(const VBN & a, const VBN & b);
	friend bool operator!=(const VBN & a, const BN_ULONG & b);
	friend bool operator!=(const BN_ULONG & a, const VBN & b);
	friend bool operator<(const VBN & a, const VBN & b);
	friend bool operator>(const VBN & a, const VBN & b);
	friend bool operator<=(const VBN & a, const VBN & b);
	friend bool operator>=(const VBN & a, const VBN & b);

	VBN & operator>>=(int b);
	VBN & operator<<=(int b);
	friend VBN operator>>(const VBN & a, int b);
	friend VBN operator<<(const VBN & a, int b);

	friend std::istream & operator>>(std::istream & in, VBN & vbn);
	friend std::ostream & operator<<(std::ostream & out, VBN & vbn);

	// Варианты операторов, определяемые через VBN_DEFAULT_UNSIGNED. Одновременно определить два типа нельзя - неопределенность.
	VBN & operator=(const VBN_LONG & other);
	friend VBN operator+(const VBN & augend, const VBN_LONG & addend);
	VBN & operator+=(const VBN_LONG & addend);
	friend VBN operator-(const VBN & minuend, const VBN_LONG & subtrahend);
	VBN & operator-=(const VBN_LONG & subtrahend);
	friend VBN operator*(const VBN & multiplicand, const VBN_LONG & multiplier);
	VBN & operator*=(const VBN_LONG & multiplier);
	friend VBN operator/(const VBN & dividend, const VBN_LONG & divisor);
	VBN & operator/=(const VBN_LONG & divisor);
	friend BN_ULONG operator%(const VBN & dividend, const VBN_LONG & modulus);

#endif // VBN_DISABLE_ALL_OPERATORS

private:
	BN_CTX * getctx();
	BN_CTX * ctx;
};

/**
    VCTX - voaoom's CTX
	Этот класс используется только для автоматического управления памятью.
*/
class VCTX {
public:
	BN_CTX * const ctx;
public:
	VCTX();
	operator BN_CTX * () const;
	~VCTX();
	// Следование рекомендациям. Они не работает.
	VCTX(const VCTX & ctx); // Создает новый экземпляр не зависимо от ctx
	const VCTX & operator=(const VCTX & ctx) const; // Ничего не делает, возвращат себя.
};

#include "VBN.hpp"
#endif // VOABIGNUM_H
/* THE END */
