// Здесь компелируемая часть реализации VBN.

#include "VBN.h"
#include <openssl/bn.h>
#include <openssl/err.h>
#include <memory.h>
#include <stdexcept>

/* VBN */

VBN::VBN() : bn(BN_new()), ctx(NURUPO) {
	if(!bn) throw ERR_peek_error();
}

VBN::VBN(const VBN & other) : bn(BN_dup(other.bn)), ctx(NURUPO) {
	if(!bn) throw ERR_peek_error();
}

VBN::VBN(const BIGNUM * _bn) : bn(BN_dup(_bn)), ctx(NURUPO) {
	if(!bn) throw ERR_peek_error();
}

VBN::VBN(const char * str) : bn(BN_new()), ctx(NURUPO) {
	if(!bn || !BN_dec2bn(&bn, str)) throw ERR_peek_error();
}

VBN::VBN(const VBN_LONG & other) : bn(BN_new()), ctx(NURUPO) {
	if(!bn) throw ERR_peek_error();
	this->add(other);
}

VBN & VBN::operator=(const VBN & other) {
	if(this != &other) if(!BN_copy(bn, other.bn)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::operator=(const BIGNUM * _bn) {
	if(bn != _bn) if(!BN_copy(bn, _bn)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::operator=(const char * str) {
	if(!BN_dec2bn(&bn, str)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::operator=(const VBN_LONG & other) {
	BN_clear(bn);
	this->add(other);
	return *this;
}

#if VBN_CPP11
VBN::VBN(VBN && tmp) : bn(tmp.bn), ctx(tmp.ctx) {
	tmp.bn = NURUPO; tmp.ctx = NURUPO;
}

VBN & VBN::operator=(VBN && tmp) {
	if(bn) BN_clear_free(bn);
	if(ctx) BN_CTX_free(ctx);
	bn = tmp.bn;
	ctx = tmp.ctx;
	tmp.bn = NURUPO;
	tmp.ctx = NURUPO;
	return *this;
}
#endif // VBN_CPP11

VBN::~VBN() {
	if(bn) BN_clear_free(bn);
	if(ctx) BN_CTX_free(ctx);
}

BN_CTX * VBN::getctx() {
	if(!ctx) ctx = BN_CTX_new();
	if(!ctx) throw ERR_peek_error();
	return ctx;
}

VBN & VBN::add(const VBN & b) {
	if(!BN_add(this->bn, this->bn, b.bn)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::addu(const BN_ULONG & b) {
	if(!BN_add_word(this->bn, b)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::adds(const BN_LONG & b) {
	if(!(b > 0 ? BN_add_word(this->bn, (BN_ULONG) b) : BN_sub_word(this->bn, (BN_ULONG) - b))) throw ERR_peek_error();
	return *this;
}

VBN & VBN::mod_add(const VBN & b, const VBN & mod, BN_CTX * ctx) {
	BIGNUM * t = BN_dup(this->bn);
	if(!t || !BN_mod_add(t, this->bn, b.bn, mod.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::sub(const VBN & b) {
	BIGNUM * t = BN_new();
	if(!t || !BN_sub(t, this->bn, b.bn)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::subu(const BN_ULONG & b) {
	if(!BN_sub_word(this->bn, b)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::subs(const BN_LONG & b) {
	if(!(b > 0 ? BN_sub_word(this->bn, (BN_ULONG) b) : BN_add_word(this->bn, (BN_ULONG) - b))) throw ERR_peek_error();
	return *this;
}

VBN & VBN::mod_sub(const VBN & b, const VBN & mod, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_mod_sub(t, this->bn, b.bn, mod.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::mul(const VBN & b, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_mul(t, this->bn, b.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::mulu(const BN_ULONG & b) {
	if(!BN_mul_word(this->bn, b)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::muls(const BN_LONG & b) {
	if(b > 0) {
		if(BN_mul_word(this->bn, (BN_ULONG) b)) throw ERR_peek_error();
	} else {
		int sign = BN_is_negative(this->bn);
		if(BN_mul_word(this->bn, (BN_ULONG) - b)) throw ERR_peek_error();
		BN_set_negative(this->bn, !sign);
	}
	return *this;
}

VBN & VBN::mod_mul(const VBN & b, const VBN & mod, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_mod_mul(t, this->bn, b.bn, mod.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::div(const VBN & b, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_div(t, NURUPO, this->bn, b.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::divu(const BN_ULONG & b) {
	if(!BN_div_word(this->bn, b)) throw ERR_peek_error();
	return *this;
}

VBN & VBN::divs(const BN_LONG & b) {
	if(b > 0) {
		if(!BN_div_word(this->bn, (BN_ULONG) b)) throw ERR_peek_error();
	} else {
		int sign = BN_is_negative(this->bn);
		if(!BN_div_word(this->bn, (BN_ULONG) - b)) throw ERR_peek_error();
		BN_set_negative(this->bn, !sign);
	}
	return *this;
}

VBN & VBN::mod(const VBN & mod, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_div(NURUPO, t, this->bn, mod.bn, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::exp(const VBN & exp, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_exp(t, this->bn, exp, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::mod_exp(const VBN & exp, const VBN & mod, BN_CTX * ctx) {
	BIGNUM * t = BN_new();
	if(!t || !BN_mod_exp(t, this->bn, exp, mod, ctx)) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::sqr() {
	if(!BN_sqr(this->bn, this->bn, this->getctx())) throw ERR_peek_error();
	return *this;
}

VBN & VBN::mod_sqr(const VBN & mod) {
	BIGNUM * t = BN_new();
	if(!t || !BN_mod_sqr(t, this->bn, mod.bn, this->getctx())) {
		BN_clear_free(t);
		throw ERR_peek_error();
	}
	BN_clear_free(this->bn);
	this->bn = t;
	return *this;
}

VBN & VBN::gcd(const VBN & b) {
	if(!BN_gcd(this->bn, this->bn, b.bn, this->getctx())) throw ERR_peek_error();
	return *this;
}

VBN & VBN::lcm(const VBN & b) {
	BIGNUM * t1 = BN_new(), * t2 = BN_new();
	bool success = t1 && t2 && BN_gcd(t1, this->bn, b.bn, this->getctx()) && BN_div(t2, NURUPO, this->bn, t1, this->getctx());
	BN_clear_free(t1);
	BN_clear_free(t2);
	if(!success) throw ERR_peek_error();
	BN_clear_free(this->bn);
	this->bn = t2;
	return *this;
}

VBN & VBN::read(std::istream & in) {
	char buffer[2048]; // TODO: detect buffer size
	in >> buffer;
	if(!BN_dec2bn(&(this->bn), buffer)) {
		memset(buffer, 0x00, strlen(buffer));
		throw ERR_peek_error();
	}
	memset(buffer, 0x00, strlen(buffer));
	return *this;
}

VBN & VBN::write(std::ostream & out) {
	char * buffer = BN_bn2dec(this->bn);
	if(!buffer) throw ERR_peek_error();
	out << buffer;
	memset(buffer, 0x00, strlen(buffer));
	OPENSSL_free(buffer);
	return *this;
}

/* VCTX */

VCTX::VCTX() : ctx(BN_CTX_new()) {
	if(!ctx) throw ERR_peek_error();
}

VCTX::~VCTX() {
	if(ctx) BN_CTX_free(ctx);
}

const VCTX & VCTX::operator=(const VCTX &) const {
	return *this;
}

VCTX::VCTX(const VCTX &) : ctx(BN_CTX_new()) {
	if(!ctx) throw ERR_peek_error();
}

/* THE END */
