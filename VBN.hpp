// Здесь inline часть реализации VBN. Должно подключаться в и использовать VBN.h

inline VBN & VBN::mod_add(const VBN & addend, const VBN & mod) { return this->mod_add(addend, mod, this->getctx()); }
inline VBN add(const VBN & augend, const VBN & addend) { return VBN(augend).add(addend); }
inline VBN addu(const VBN & augend, const BN_ULONG & addend) { return VBN(augend).addu(addend); }
inline VBN adds(const VBN & augend, const BN_LONG & addend) { return VBN(augend).adds(addend); }
inline VBN mod_add(const VBN & augend, const VBN & addend, const VBN & mod, BN_CTX * ctx) { return VBN(augend).mod_add(addend, mod, ctx); }
inline VBN mod_add(const VBN & augend, const VBN & addend, const VBN & mod) { return VBN(augend).mod_add(addend, mod); }

inline VBN & VBN::mod_sub(const VBN & subtrahend, const VBN & mod) { return this->mod_sub(subtrahend, mod, this->getctx()); }
inline VBN sub(const VBN & minuend, const VBN & subtrahend) { return VBN(minuend).sub(subtrahend); }
inline VBN subu(const VBN & minuend, const BN_ULONG & subtrahend) { return VBN(minuend).subu(subtrahend); }
inline VBN subs(const VBN & minuend, const BN_LONG & subtrahend) { return VBN(minuend).subs(subtrahend); }
inline VBN mod_sub(const VBN & minuend, const VBN & subtrahend, const VBN & mod, BN_CTX * ctx) { return VBN(minuend).mod_sub(subtrahend, mod, ctx); }
inline VBN mod_sub(const VBN & minuend, const VBN & subtrahend, const VBN & mod) { return VBN(minuend).mod_sub(subtrahend, mod); }

inline VBN & VBN::mul(const VBN & multiplier) { return this->mul(multiplier, this->getctx()); }
inline VBN & VBN::mod_mul(const VBN & multiplier, const VBN & mod) { return this->mod_mul(multiplier, mod, this->getctx()); }
inline VBN mul(const VBN & multiplicand, const VBN & multiplier) { return VBN(multiplicand).mul(multiplier); }
inline VBN mul(const VBN & multiplicand, const VBN & multiplier, BN_CTX * ctx) { return VBN(multiplicand).mul(multiplier, ctx); }
inline VBN mulu(const VBN & multiplicand, const BN_ULONG & multiplier) { return VBN(multiplicand).mulu(multiplier); }
inline VBN muls(const VBN & multiplicand, const BN_LONG & multiplier) { return VBN(multiplicand).muls(multiplier); }
inline VBN mod_mul(const VBN & multiplicand, const VBN & multiplier, const VBN & mod, BN_CTX * ctx) { return VBN(multiplicand).mod_mul(multiplier, mod, ctx); }
inline VBN mod_mul(const VBN & multiplicand, const VBN & multiplier, const VBN & mod) { return VBN(multiplicand).mod_mul(multiplier, mod); }

inline VBN & VBN::div(const VBN & divisor) { return this->div(divisor, this->getctx()); }
inline VBN div(const VBN & dividend, const VBN & divisor) { return VBN(dividend).div(divisor); }
inline VBN div(const VBN & dividend, const VBN & divisor, BN_CTX * ctx) { return VBN(dividend).div(divisor, ctx); }
inline VBN divu(const VBN & dividend, const BN_ULONG & divisor) { return VBN(dividend).divu(divisor); }
inline VBN divs(const VBN & dividend, const BN_LONG & divisor) { return VBN(dividend).divs(divisor); }

inline VBN & VBN::mod(const VBN & mod) { return this->mod(mod, this->getctx()); }
inline BN_ULONG VBN::modu(const BN_ULONG & mod) const { return BN_mod_word(this->bn, mod); }
inline BN_ULONG VBN::mods(const BN_LONG & mod) const { return BN_mod_word(this->bn, (BN_ULONG)(mod > 0 ? mod : -mod)); }
inline VBN mod(const VBN & dividend, const VBN & mod, BN_CTX * ctx) { return VBN(dividend).mod(mod, ctx); }
inline VBN mod(const VBN & dividend, const VBN & mod) { return VBN(dividend).mod(mod); }
inline BN_ULONG modu(const VBN & dividend, const BN_ULONG & mod) { return VBN(dividend).modu(mod); }
inline BN_ULONG mods(const VBN & dividend, const BN_LONG & mod) { return VBN(dividend).mods(mod); }

inline VBN & VBN::exp(const VBN & exp) { return this->exp(exp, this->getctx()); }
inline VBN & VBN::mod_exp(const VBN & exp, const VBN & mod) { return this->mod_exp(exp, mod, this->getctx()); }
inline VBN exp(const VBN & base, const VBN & exp) { return VBN(base).exp(exp); }
inline VBN exp(const VBN & base, const VBN & exp, BN_CTX * ctx) { return VBN(base).exp(exp, ctx); }
inline VBN mod_exp(const VBN & base, const VBN & exp, const VBN & mod) { return VBN(base).mod_exp(exp, mod); }
inline VBN mod_exp(const VBN & base, const VBN & exp, const VBN & mod, BN_CTX * ctx) { return VBN(base).mod_exp(exp, mod, ctx); }

inline VBN sqr(const VBN & a) { return VBN(a).sqr(); }
inline VBN sqr(const VBN & a, const VBN & mod) { return VBN(a).mod_sqr(mod); }

inline VBN gcd(const VBN & a, const VBN & b) { return VBN(a).gcd(b); }
inline VBN lcm(const VBN & a, const VBN & b) { return VBN(a).lcm(b); }

inline VBN & VBN::neg() { BN_set_negative(this->bn, BN_is_negative(this->bn)); return *this; }
inline VBN & VBN::inc() { BN_add_word(this->bn, 1); return *this; }
inline VBN & VBN::dec() { BN_sub_word(this->bn, 1); return *this; }
inline VBN neg(const VBN & a) { return VBN(a).neg(); }
inline VBN inc(const VBN & a) { return VBN(a).inc(); }
inline VBN dec(const VBN & a) { return VBN(a).dec(); }

inline bool VBN::is_zero() const { return BN_is_zero(this->bn); }
inline bool VBN::is_one() const { return BN_is_one(this->bn); }
inline bool VBN::is_odd() const { return BN_is_odd(this->bn); }
inline bool VBN::is(const BN_ULONG & u_word) const { return BN_is_word(this->bn, u_word); }
// TODO: check!
inline bool VBN::is(const BN_LONG & s_word) const { return ((s_word < 0) == (bool) BN_is_negative(this->bn)) ? BN_abs_is_word(this->bn, (BN_ULONG) s_word) : false; }

inline VBN & VBN::rshift(int b) { BN_rshift(this->bn, this->bn, b); return *this; }
inline VBN & VBN::lshift(int b) { BN_lshift(this->bn, this->bn, b); return *this; }
inline VBN & VBN::rshift1() { BN_rshift1(this->bn, this->bn); return *this; }
inline VBN & VBN::lshift1() {  BN_lshift1(this->bn, this->bn); return *this; }
inline VBN rshift(const VBN & a, int b) { return VBN(a).rshift(b); }
inline VBN lshift(const VBN & a, int b) { return VBN(a).lshift(b); }
inline VBN rshift1(const VBN & a) { return VBN(a).rshift1(); }
inline VBN lshift1(const VBN & a) { return VBN(a).lshift1(); }

#ifndef VBN_DISABLE_ALL_OPERATORS

inline VBN::operator BIGNUM & () const { return *bn; }
inline VBN::operator BIGNUM * () const { return bn; }
inline VBN::operator BIGNUM * const * () const { return &bn; }
inline VBN::operator bool () const { return !this->is_zero(); }

inline VBN operator+(const VBN & augend, const VBN & addend) { return add(augend, addend); }
inline VBN & VBN::operator+=(const VBN & addend) { return this->add(addend); }

inline VBN operator-(const VBN & minuend, const VBN & subtrahend) { return sub(minuend, subtrahend); }
inline VBN & VBN::operator-=(const VBN & subtrahend) { return this->sub(subtrahend); }

inline VBN operator*(const VBN & multiplicand, const VBN & multiplier) { return mul(multiplicand, multiplier); }
inline VBN & VBN::operator*=(const VBN & multiplier) { return this->mul(multiplier); }

inline VBN operator/(const VBN & dividend, const VBN & divisor) { return div(dividend, divisor); }
inline VBN & VBN::operator/=(const VBN & divisor) { return this->div(divisor); }

inline VBN operator%(const VBN & dividend, const VBN & mod) { return VBN(dividend).mod(mod); }
inline VBN & VBN::operator%=(const VBN & mod) { return this->mod(mod); }

inline VBN operator^(const VBN & base, const VBN & exp) { return VBN(base).exp(exp); }
inline VBN & VBN::operator^=(const VBN & exp) { return this->exp(exp); }

inline VBN operator&(const VBN & a, const VBN & b) { return gcd(a, b); }
inline VBN operator|(const VBN & a, const VBN & b) { return lcm(a, b); }
inline VBN & VBN::operator&=(const VBN & b) { return this->gcd(b); }
inline VBN & VBN::operator|=(const VBN & b) { return this->lcm(b); }

inline VBN operator-(const VBN & a) { return neg(a); }
inline VBN & VBN::operator++() { return this->inc(); }
inline VBN & VBN::operator--() { return this->dec(); }
inline VBN operator++(VBN & a, int) { VBN t(a); a.inc(); return t; }
inline VBN operator--(VBN & a, int) { VBN t(a); a.dec(); return t; }

inline bool VBN::operator!() const { return this->is_zero(); }

inline bool operator==(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) == 0; }
inline bool operator==(const VBN & a, const BN_ULONG & b) { return a.is(b); }
inline bool operator==(const BN_ULONG & a, const VBN & b) { return b.is(a); }
inline bool operator!=(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) != 0; }
inline bool operator!=(const VBN & a, const BN_ULONG & b) { return !a.is(b); }
inline bool operator!=(const BN_ULONG & a, const VBN & b) { return !b.is(a); }
inline bool operator<(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) < 0; }
inline bool operator>(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) > 0; }
inline bool operator<=(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) <= 0; }
inline bool operator>=(const VBN & a, const VBN & b) { return BN_cmp(a.bn, b.bn) >= 0; }

inline VBN & VBN::operator>>=(int b) { return this->rshift(b); }
inline VBN & VBN::operator<<=(int b) { return this->lshift(b); }

inline VBN operator>>(const VBN & a, int b) { return rshift(a, b); }
inline VBN operator<<(const VBN & a, int b) { return lshift(a, b); }

inline std::istream & operator>>(std::istream & in, VBN & vbn) { vbn.read(in); return in; }
inline std::ostream & operator<<(std::ostream & out, VBN & vbn) { vbn.write(out); return out; }

#ifdef VBN_DEFAULT_UNSIGNED

inline VBN operator+(const VBN & augend, const BN_ULONG & addend) { return addu(augend, addend); }
inline VBN & VBN::operator+=(const BN_ULONG & addend) { return this->addu(addend); }

inline VBN operator-(const VBN & minuend, const BN_ULONG & subtrahend) { return subu(minuend, subtrahend); }
inline VBN & VBN::operator-=(const BN_ULONG & subtrahend) { return this->subu(subtrahend); }

inline VBN operator*(const VBN & multiplicand, const BN_ULONG & multiplier) { return mulu(multiplicand, multiplier); }
inline VBN & VBN::operator*=(const BN_ULONG & multiplier) { return this->mulu(multiplier); }

inline VBN operator/(const VBN & dividend, const BN_ULONG & divisor) { return divu(dividend, divisor); }
inline VBN & VBN::operator/=(const BN_ULONG & divisor) { return this->divu(divisor); }

inline BN_ULONG operator%(const VBN & dividend, const BN_ULONG & mod) { return modu(dividend, mod); }

#else // VBN_DEFAULT_UNSIGNED

inline VBN operator+(const VBN & augend, const BN_LONG & addend) { return adds(augend, addend); }
inline VBN & VBN::operator+=(const BN_LONG & addend) { return this->adds(addend); }

inline VBN operator-(const VBN & augend, const BN_LONG & addend) { return subs(augend, addend); }
inline VBN & VBN::operator-=(const BN_LONG & subtrahend) { return this->subs(subtrahend); }

inline VBN operator*(const VBN & multiplicand, const BN_LONG & multiplier) { return muls(multiplicand, multiplier); }
inline VBN & VBN::operator*=(const BN_LONG & multiplier) { return this->muls(multiplier); }

inline VBN operator/(const VBN & dividend, const BN_LONG & divisor) { return divs(dividend, divisor); }
inline VBN & VBN::operator/=(const BN_LONG & divisor) { return this->divs(divisor); }

inline BN_ULONG operator%(const VBN & dividend, const BN_LONG & mod) { return mods(dividend, mod); }

#endif // VBN_DEFAULT_UNSIGNED

#endif // VBN_DISABLE_ALL_OPERATORS

/* VCTX */

inline VCTX::operator BN_CTX * () const { return this->ctx; }

/* THE END */
